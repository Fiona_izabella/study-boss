<?php
// array for JSON response
$response = array();
 
// include db connect class
//require_once __DIR__ . '/db_connect.php';

define('__ROOT__', dirname(dirname(__FILE__))); 
require_once(__ROOT__.'/studyBoss/db_connect.php');
 
// connecting to db
$db = new DB_CONNECT();
 
// get all tasks from taskInfo table
$result = mysql_query("SELECT *FROM taskInfo ORDER BY task_date asc") or die(mysql_error());
 
// check for empty result
if (mysql_num_rows($result) > 0) {
    // looping through all results
    // tasks node
    $response["tasks"] = array();
 
    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $task = array();
       $task["pid"] = $row["task_id"];
        $task["title"] = $row["task_title"];
        $task["date"] = $row["task_date"];
        $task["detail"] = $row["task_detail"];
         $task["priority"] = $row["task_priority"];
           $task["complete"] = $row["complete"];
           $task["notification"] = $row["notification"];
        $task["created_at"] = $row["task_created"];
      
 
        // push single task into final response array
        array_push($response["tasks"], $task);
    }
    // success
    $response["success"] = 1;
 
    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "No tasks found";
 
    // echo no users JSON
    echo json_encode($response);
}
?>
