# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.25)
# Database: studyBoss
# Generation Time: 2014-04-08 09:22:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table taskInfo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `taskInfo`;

CREATE TABLE `taskInfo` (
  `task_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `task_date` varchar(100) NOT NULL DEFAULT '',
  `task_title` varchar(22) DEFAULT NULL,
  `task_detail` varchar(145) DEFAULT '',
  `task_priority` varchar(11) DEFAULT '',
  `complete` varchar(5) DEFAULT NULL,
  `notification` varchar(5) DEFAULT NULL,
  `task_created` datetime DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

LOCK TABLES `taskInfo` WRITE;
/*!40000 ALTER TABLE `taskInfo` DISABLE KEYS */;

INSERT INTO `taskInfo` (`task_id`, `task_date`, `task_title`, `task_detail`, `task_priority`, `complete`, `notification`, `task_created`)
VALUES
	(1,'3-June-2014','android work','lots to do\n','high','true','true',NULL),
	(2,'17-April-2014','paul78','testing','','true','true',NULL),
	(12,'7-April-2014','horse','Read the horse book','','false','true',NULL),
	(14,'13-April-2014','beetroot','eat beetroot','','false','true',NULL),
	(15,'7-March-2014','englsih','title Herefordshire','medium','true','true',NULL),
	(21,'3-April-2014','History','do history notes','high','false','true',NULL),
	(24,'6-April-2014','goal','do list','high','true','true',NULL),
	(25,'4-April-2014','till receipts','till receipts','','true','true',NULL),
	(27,'7-May-2014','java notes','re read notes','medium','false','true',NULL),
	(28,'17-April-2014','fiona break','do all my todo lists','high','false','true',NULL);

/*!40000 ALTER TABLE `taskInfo` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
