package com.example.studyBoss;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
//import the async
import com.AsyncTasks.GetNotificationResults;

//need to implement onClicklstener so we can use its functionality
//implement the AsyncResponse and add its methods
public class MainScreenActivity extends Activity implements OnClickListener,
		AsyncResponse {

	private Button btnViewTasks;
	private Button btnTaskCalendar;
	private Button btnCalendar;
	private Button btnNewTasks;
	private Button headerBtn;
	private Button homeBtn;
	private TextView mDisplayCurrentDate;
	private int mYear;
	private int mMonth;
	private int mDay;
	private int month;
	public String getDateText;
	public String newDatesNotificationArray;
	public ArrayList<String> newDatesNotificationArrayString;
	private static Boolean mNotificationDate = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// display the custom title bar
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.main_screen);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.header);

		// display current date
		mDisplayCurrentDate = (TextView) findViewById(R.id.display_Date);

		final Calendar c = Calendar.getInstance();
		Date date = c.getTime();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		c.set(Calendar.YEAR, mYear);
		c.set(Calendar.MONTH, mMonth);
		c.set(Calendar.DAY_OF_MONTH, mDay);
		// set the date format
		String myFormat = "dd-MMMM-yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);
		mDisplayCurrentDate.setText("Today is " + sdf.format(c.getTime()));
		getDateText = sdf.format(c.getTime());

		// create new instance of GetNotificationResults
		GetNotificationResults asyncTask = new GetNotificationResults();
		asyncTask.delegate = this;
		// Start Async task
		asyncTask.execute();

		// Logs for testing
		// Log.d("nmainscreen notification dates***", "notification dates "
		// + newDatesNotificationArrayString);
		// Log.d("mNotificationDate***", "mNotificationDate " +
		// mNotificationDate);
		// Log.d("getDateTexte***", "getDateText " + getDateText);

		// Buttons. Assign to a view button
		btnViewTasks = (Button) findViewById(R.id.btnViewTasks);
		btnNewTasks = (Button) findViewById(R.id.btnCreateTask);
		btnCalendar = (Button) findViewById(R.id.btnCalendar);
		headerBtn = (Button) findViewById(R.id.header_right_btn);
		homeBtn = (Button) findViewById(R.id.header_right_lft);

		// need to set the onclick listeneners
		btnViewTasks.setOnClickListener(this);
		btnNewTasks.setOnClickListener(this);
		btnCalendar.setOnClickListener(this);
		headerBtn.setOnClickListener(this);
		homeBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnViewTasks:

			// Launching TasksListActivity
			Intent i = new Intent(getApplicationContext(),
					TaskListActivity.class);

			startActivity(i);
			break;

		case R.id.btnCreateTask:

			Intent j = new Intent(getApplicationContext(),
					NewTaskActivity.class);
			startActivity(j);

			break;

		case R.id.btnCalendar:

			Intent k = new Intent(getApplicationContext(),
					CalendarActivity.class);
			startActivity(k);
			break;
		// title bar button
		case R.id.header_right_btn:
			Intent l = new Intent(getApplicationContext(),
					CalendarActivity.class);
			startActivity(l);

			break;
		// title bar button
		case R.id.header_right_lft:
			Intent m = new Intent(getApplicationContext(),
					MainScreenActivity.class);
			startActivity(m);
			break;

		}
	}

	@Override
	public void processFinish(String output) {
		// TODO Auto-generated method stub

	}

	@Override
	// get the results from the on post execute method of
	// getNotificationResults' and pass back to MainScreenActivity
	public void processFinishNotification(String outputNotification) {
		newDatesNotificationArray = outputNotification;
		// change into normal string array so we can loop through the data and
		// use an if statement

		if (newDatesNotificationArray != null) {

			newDatesNotificationArray = newDatesNotificationArray.substring(1,
					newDatesNotificationArray.length() - 1);
			newDatesNotificationArrayString = new ArrayList<String>(
					Arrays.asList(newDatesNotificationArray.split(",[ ]*")));

		}

		/**
		 * loop through the dates with notification set. if current date
		 * (getDateText) EQUALS a notification date from database (n) then
		 * change value of Boolean 'mDateOnTask' to true.
		 */

		for (String n : newDatesNotificationArrayString) {

			if (getDateText.trim().equals(n)) {

				mNotificationDate = true;

				System.out.println("Notifcation dates from database" + n);

			} else {

				mNotificationDate = false;
			}

			// end of if statement
			// call the addNotification method
			addNotification("You have a task due today", "Go to Tasks");
		}// end of for loop

		// make a toast with dates array for testing and debugging
		//Toast.makeText(MainScreenActivity.this, "notification dates" + newDatesNotificationArray,
				//Toast.LENGTH_LONG).show();

	}// end of processFinishNotification()

	// If there the is a task due on the current date and it IS SET for a
	// notification, then the mDateOnTask will equal true.
	public void addNotification(String a, String b) {

		if (mNotificationDate == true) {

			Notify(a, b);
		}
	}

	// Notify method to create a notification
	@SuppressWarnings("deprecation")
	private void Notify(String notificationTitle, String notificationMessage) {
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		@SuppressWarnings("deprecation")
		Notification notification = new Notification(R.drawable.explanation,
				"you have a new task", System.currentTimeMillis());

		Intent notificationIntent = new Intent(this, TaskListActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);
		Context context;
		notification.setLatestEventInfo(MainScreenActivity.this,
				notificationTitle, notificationMessage, pendingIntent);
		// make notification disappear once it has been selected
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(9999, notification);

	}

}// end of class
