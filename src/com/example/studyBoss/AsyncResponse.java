package com.example.studyBoss;

public interface AsyncResponse {
	// pass results to CalendarActivity and MainScreen Activity which implements AsyncResponse
	void processFinish(String output);

	void processFinishNotification(String output);
}