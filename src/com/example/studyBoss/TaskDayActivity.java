package com.example.studyBoss;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class TaskDayActivity extends ListActivity {

	// Progress Dialog
	private ProgressDialog pDialog;
	// Creating JSON Parser object
	JSONParser jParser = new JSONParser();
	ArrayList<HashMap<String, String>> tasksList;
	// url to get all tasks list
	private static String url_all_tasks_by_date = "http://fionawebdesign.co.uk/studyBoss/get_tasks_date.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_taskS = "tasks";
	private static final String TAG_PID = "pid";
	private static final String TAG_TASKDATE = "date";
	private static final String TAG_NAME = "title";
	private static final String TAG_COMPLETE_TASK = "complete";
	private static final String TAG_PRIORITY = "priority";

	public String tdate;
	public String testdate;
	private Button addTaskBtn;
	private Button btnCancel;
	public String resultAsString = null;
	public ListView lv;
	private String mPriorityResult;
	private String priority;
	private String todaysDate;
	private String complete;
	private String date;
	private String title;
	private String id;
	private boolean completeResult;
	private String completeResultTest;
	private CheckBox mCheckBoxComplete;

	// tasks JSONArray
	JSONArray tasks = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.all_day_tasks);
		// buttons
		addTaskBtn = (Button) findViewById(R.id.btnCreateTask);
		btnCancel = (Button) findViewById(R.id.btnCancelTask);

		// get intent
		Intent i = getIntent();
		// getting task date from intent, so that we can find tasks for that
		// date from
		// remote database
		tdate = i.getStringExtra(TAG_TASKDATE);

		// Hashmap for ListView
		tasksList = new ArrayList<HashMap<String, String>>();

		// Load tasks in Background Thread
		new LoadAllTasks().execute();

		// Get listview
		lv = getListView();

		// cancel button
		btnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// starting background task to update task
				Intent j = new Intent(getApplicationContext(),
						CalendarActivity.class);

				startActivity(j);
			}
		});

		// add Task button
		addTaskBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// starting background task to update task
				Intent i = new Intent(getApplicationContext(),
						NewTaskByDateActivity.class);
				i.putExtra(TAG_TASKDATE, tdate);
				startActivity(i);
			}
		});

		// Select a single task to open EditTask Activity for that particular
		// task.
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// get ID from selected ListItem
				String pid = ((TextView) view.findViewById(R.id.pid)).getText()
						.toString();

				// Start new intent
				Intent in = new Intent(getApplicationContext(),
						EditTaskActivity.class);
				// sending pid (iD) to next EditTaskActivity
				in.putExtra(TAG_PID, pid);

				// Start new activity and expecting some response back
				startActivityForResult(in, 100);
			}
		});

	}

	// Response from EditTaskActivity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received, means the task was
			// edited/deleted. Reload this screen.
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		} else {

		}

	}

	/**
	 * Background 'LoadAllTasks' Async Task to Load all tasks by making HTTP
	 * Request to database made up of onPreExecute() , doInBackground() and
	 * onPostExecute().
	 * */
	class LoadAllTasks extends AsyncTask<String, String, String> {

		// show progress dialogue
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(TaskDayActivity.this);
			pDialog.setMessage("Loading tasks. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		// Get all tasks
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// pass the value we get from intent from CalendarActivity
			// second parameter is the value you want to add, 1st parameter is
			// the file in POST.
			params.add(new BasicNameValuePair("date", tdate));

			// getting JSON string from URL as a result
			JSONObject json = jParser.makeHttpRequest(url_all_tasks_by_date,
					"POST", params);

			// Check your log cat for JSON reponse
			// Log.d("All Tasks: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {

					resultAsString = "worked";
					// tasks found

					// Getting Array of tasks
					tasks = json.getJSONArray(TAG_taskS);

					// looping through All tasks
					for (int i = 0; i < tasks.length(); i++) {
						JSONObject c = tasks.getJSONObject(i);

						// Storing each json item in variable
						id = c.getString(TAG_PID);
						title = c.getString(TAG_NAME);
						date = c.getString(TAG_TASKDATE);
						priority = c.getString(TAG_PRIORITY);
						complete = c.getString(TAG_COMPLETE_TASK);
						completeResult = Boolean.valueOf(complete);

						// set the set we want to see in the view for whether a
						// task is
						// complete or not
						if (completeResult) {
							completeResultTest = "Complete";
						} else {
							completeResultTest = "Need to do!";
						}

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_PID, id);
						map.put(TAG_NAME, title);
						map.put(TAG_TASKDATE, date);
						map.put(TAG_PRIORITY, priority);
						map.put(TAG_COMPLETE_TASK, completeResultTest);

						// adding HashList to tasksList Array
						tasksList.add(map);

					}
				} else {

				}
			} catch (JSONException e) {

				// return e.printStackTrace();
				e.printStackTrace();
			}

			// return String value for use in Toast activation
			return resultAsString;

		}

		/**
		 * Dismiss the progress dialog and set up custom Toast activation
		 * depending on value returned from doInBackground then run the runnable
		 * method and use the custom simple adapter to create and fill the
		 * listView values.
		 */

		protected void onPostExecute(String result) {
			pDialog.dismiss();

			if (result == "worked") {

				LayoutInflater inflater = getLayoutInflater();
				// Inflate the Layout
				View layout = inflater.inflate(R.layout.my_custom_toast,
						(ViewGroup) findViewById(R.id.custom_toast_layout));

				TextView text = (TextView) layout.findViewById(R.id.textToShow);
				// Set the Text to show in TextView
				text.setText("You have tasks today");

				Toast toast = new Toast(getApplicationContext());
				toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0,
						0);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();

			} else {

				LayoutInflater inflater = getLayoutInflater();
				// Inflate the Layout
				View layout = inflater.inflate(R.layout.my_custom_toast,
						(ViewGroup) findViewById(R.id.custom_toast_layout));

				TextView text = (TextView) layout.findViewById(R.id.textToShow);
				// Set the Text to show in TextView
				text.setText("You have no tasks set for this day. Select 'Add task' to create a new task");

				Toast toast = new Toast(getApplicationContext());
				toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0,
						0);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();

			}

			runOnUiThread(new Runnable() {
				public void run() {
					// Updating parsed JSON data into ListView
					// create new custom adapter
					ListAdapter adapter = new CustomAdapter(
							TaskDayActivity.this, tasksList,
							R.layout.list_item, new String[] { TAG_PID,
									TAG_NAME, TAG_TASKDATE, TAG_COMPLETE_TASK,
									TAG_PRIORITY },

							new int[] { R.id.pid, R.id.title,
									R.id.task_list_item_dateTextView,
									R.id.task_list_item_complete,
									R.id.priority_text });

					setListAdapter(adapter);

				}

			});

		}// end of onPostexecute

	}// end of loadAlltasks Async Class

	/* ===== custom adapter ======= */
	public class CustomAdapter extends SimpleAdapter {
		public CustomAdapter(Context context,
				List<? extends Map<String, ?>> data, int resource,
				String[] from, int[] to) {
			super(context, data, resource, from, to);

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = super.getView(position, convertView, parent);
			Object item = getItem(position);
			ColorStateList color = getResources().getColorStateList(
					R.drawable.text_color);
			ColorStateList colorHighlight = getResources().getColorStateList(
					R.drawable.text_colour_emphasize);

			// get listView and adapter count
			int countChild = lv.getChildCount();
			int adapterCount = lv.getCount();

			// loop through adapter list count to find listview values
			for (int i = 0; i < adapterCount; i++) {

				TextView priorityText = (TextView) v
						.findViewById(R.id.priority_text);
				TextView completeState = (TextView) v
						.findViewById(R.id.task_list_item_complete);
				String p = (String) priorityText.getText();
				String c = (String) completeState.getText();

				/**
				 * change drawable background resources of list view depending
				 * on listItem values. Need 'else' solution, otherwise the first
				 * item in list doesn't respond properly
				 */

				if (p.equalsIgnoreCase("high")) {
					priorityText.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.high_circle, 0);

				} else if (p.equalsIgnoreCase("medium")) {
					priorityText.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.exclamation_mid, 0);

				} else {

					priorityText.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.exclamation_circle, 0);
				}

				if (c.equalsIgnoreCase("Complete")) {

					completeState.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.star_green, 0);

				} else {

					completeState.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.star_o, 0);

				}

			}

			return v;
		}
	}

}// end of class