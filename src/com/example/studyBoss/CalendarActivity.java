package com.example.studyBoss;

import java.sql.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import 2 async classes
import com.AsyncTasks.GetNotificationResults;
import com.AsyncTasks.Load_dates;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.webkit.WebView;

//need to implement the onClickListener and the asynResponse
@TargetApi(3)
public class CalendarActivity extends Activity implements OnClickListener,
		AsyncResponse {

	private static final String tag = "MyCalendarActivity";
	private static final String TAG_DATES = "dates";
	private static final String TAG_SINGLEDATE = "single_date";
	private static final String TAG_TASKDATE = "date";
	private static final String TAG_SUCCESS = "success";
	public static String datesArray;
	private static final String TAG_DATE = "date";
	//delay for Toast
	private static final int LONG_DELAY = 10500;
	// new JSONParse object
	JSONParser jParser = new JSONParser();
	//Url to query the database
	private static String url_all_dates = "http://fionawebdesign.co.uk/studyBoss/get_dates.php";
	// JSONArray tasks
	JSONArray dates = null;
	// Progress Dialog
	private ProgressDialog pDialog;
	// buttons
	private Button selectedDayMonthYearButton;
	private Button showTasks;
	private Button showArray;
	private Button btnmenu;
	public Button gridcell;
	// string/array values
	public String newDatesArray;
	public ArrayList<String> newDatesArrayString;
	public ArrayList<HashMap<String, String>> datesList;
	public ArrayList<String> stringArray = new ArrayList<String>();
	public ArrayList<String> listAllDates;
	public String date;
	public String jsonResult;
	public String s;
	// result from doInbackground
	public String doInBackResult;
	// calendar variables
	private TextView currentMonth;
	private ImageView prevMonth;
	private ImageView nextMonth;
	private GridView calendarView;
	private GridCellAdapter adapter;
	private Calendar _calendar;
	@SuppressLint("NewApi")
	private int month, year;
	@SuppressWarnings("unused")
	@SuppressLint({ "NewApi", "NewApi", "NewApi", "NewApi" })
	private final DateFormat dateFormatter = new DateFormat();
	private static final String dateTemplate = "MMMM yyyy";
	private static final String dateFullTemplate = "dd MMMM yyyy";
	public String getgridText;

	/** onCreate..called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_calendar_view);

		/**
		 * get instance of Load_dates Async class to get results for all the
		 * task dates from teh dtabase
		 */

		Load_dates loadAlldates = new Load_dates();
		loadAlldates.delegate = this;
		// Start async task
		loadAlldates.execute();

		/*
		 * my testing logs
		 */
		//Log.d("newDatesArray***", "newDatesArray " + newDatesArray);
		//Log.d("**newDatesArrayString***", "newDatesArrayString " + newDatesArrayString);
		

		// Back to MainScreenActivity Menu Button
		btnmenu = (Button) findViewById(R.id.btnMenu);

		btnmenu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getApplicationContext(),
						MainScreenActivity.class);

				startActivity(i);
			}
		});

		// Calendar
		_calendar = Calendar.getInstance(Locale.getDefault());
		month = _calendar.get(Calendar.MONTH) + 1;
		year = _calendar.get(Calendar.YEAR);
		// display the date which is selected at the top of the page
		selectedDayMonthYearButton = (Button) this
				.findViewById(R.id.selectedDayMonthYear);
		prevMonth = (ImageView) this.findViewById(R.id.prevMonth);
		prevMonth.setOnClickListener(this);
		currentMonth = (TextView) this.findViewById(R.id.currentMonth);
		currentMonth.setText(DateFormat.format(dateTemplate,
				_calendar.getTime()));
		selectedDayMonthYearButton.setText(DateFormat.format(dateFullTemplate,
				_calendar.getTime()));
		nextMonth = (ImageView) this.findViewById(R.id.nextMonth);
		nextMonth.setOnClickListener(this);
		calendarView = (GridView) this.findViewById(R.id.calendar);

		// Initialise GridCellAdapter
		adapter = new GridCellAdapter(getApplicationContext(),
				R.id.calendar_day_gridcell, month, year);
		// update the adapter.
		adapter.notifyDataSetChanged();
		calendarView.setAdapter(adapter);

	}

	// set the grid cells
	private void setGridCellAdapterToDate(int month, int year) {
		adapter = new GridCellAdapter(getApplicationContext(),
				R.id.calendar_day_gridcell, month, year);
		_calendar.set(year, month - 1, _calendar.get(Calendar.DAY_OF_MONTH));
		currentMonth.setText(DateFormat.format(dateTemplate,
				_calendar.getTime()));
		adapter.notifyDataSetChanged();
		calendarView.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		if (v == prevMonth) {
			if (month <= 1) {
				month = 12;
				year--;
			} else {
				month--;
			}
			setGridCellAdapterToDate(month, year);
		}
		if (v == nextMonth) {
			if (month > 11) {
				month = 1;
				year++;
			} else {
				month++;
			}
			setGridCellAdapterToDate(month, year);
		}

	}

	@Override
	public void onDestroy() {
		Log.d(tag, "Destroying the View ...");
		super.onDestroy();
	}

	/**
	 * Inner Class, grid cell adapter class which extends androids
	 * BaseAdapter. Creates and updates the calendar grid. 
	 */

	public class GridCellAdapter extends BaseAdapter implements OnClickListener {
		// set variables
		private static final String tag = "GridCellAdapter";
		private final Context _context;
		private final List<String> list;
		private static final int DAY_OFFSET = 1;
		private final String[] weekdays = new String[] { "Sun", "Mon", "Tue",
				"Wed", "Thu", "Fri", "Sat" };
		private final String[] months = { "January", "February", "March",
				"April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		private final int[] daysOfMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30,
				31, 30, 31 };
		private int daysInMonth;
		private int currentDayOfMonth;
		private int currentWeekDay;
		private TextView num_events_per_day;
		private final HashMap<String, Integer> eventsPerMonthMap;
		private final SimpleDateFormat dateFormatter = new SimpleDateFormat(
				"dd-MMM-yyyy");

		// gridcellAdapter constructor
		public GridCellAdapter(Context context, int textViewResourceId,
				int month, int year) {
			super();
			this._context = context;
			this.list = new ArrayList<String>();

			Calendar calendar = Calendar.getInstance();
			setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
			setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
			// Print Month
			printMonth(month, year);
			// Find Number of Events
			eventsPerMonthMap = findNumberOfEventsPerMonth(year, month);
		}

		private String getMonthAsString(int i) {
			return months[i];
		}

		private String getWeekDayAsString(int i) {
			return weekdays[i];
		}

		private int getNumberOfDaysOfMonth(int i) {
			return daysOfMonth[i];
		}

		public String getItem(int position) {
			return list.get(position);
		}

		@Override
		public int getCount() {
			return list.size();
		}

		// Prints Month
		private void printMonth(int mm, int yy) {
			int trailingSpaces = 0;
			int daysInPrevMonth = 0;
			int prevMonth = 0;
			int prevYear = 0;
			int nextMonth = 0;
			int nextYear = 0;

			int currentMonth = mm - 1;
			String currentMonthName = getMonthAsString(currentMonth);
			daysInMonth = getNumberOfDaysOfMonth(currentMonth);

			GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);
			// Log.d(tag, "Gregorian Calendar:= " + cal.getTime().toString());

			if (currentMonth == 11) {
				prevMonth = currentMonth - 1;
				daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
				nextMonth = 0;
				prevYear = yy;
				nextYear = yy + 1;

			} else if (currentMonth == 0) {
				prevMonth = 11;
				prevYear = yy - 1;
				nextYear = yy;
				daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
				nextMonth = 1;

			} else {
				prevMonth = currentMonth - 1;
				nextMonth = currentMonth + 1;
				nextYear = yy;
				prevYear = yy;
				daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);

			}

			int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
			trailingSpaces = currentWeekDay;

			if (cal.isLeapYear(cal.get(Calendar.YEAR)))
				if (mm == 2)
					++daysInMonth;
				else if (mm == 3)
					++daysInPrevMonth;

			// Trailing Month days
			for (int i = 0; i < trailingSpaces; i++) {

				list.add(String
						.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET)
								+ i)
						+ "-GREY"
						+ "-"
						+ getMonthAsString(prevMonth)
						+ "-"
						+ prevYear);
			}

			// Current Month Days
			for (int i = 1; i <= daysInMonth; i++) {

				if (i == getCurrentDayOfMonth()) {
					list.add(String.valueOf(i) + "-BLUE" + "-"
							+ getMonthAsString(currentMonth) + "-" + yy);
				} else {
					list.add(String.valueOf(i) + "-WHITE" + "-"
							+ getMonthAsString(currentMonth) + "-" + yy);
				}
			}

			// Leading Month days
			for (int i = 0; i < list.size() % 7; i++) {

				list.add(String.valueOf(i + 1) + "-GREY" + "-"
						+ getMonthAsString(nextMonth) + "-" + nextYear);
			}
		}

		private HashMap<String, Integer> findNumberOfEventsPerMonth(int year,
				int month) {
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			return map;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		// grid cell adapter getView method
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			if (row == null) {
				LayoutInflater inflater = (LayoutInflater) _context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(R.layout.screen_gridcell, parent, false);
			}

			// **Get a reference to the Day gridcell
			gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
			gridcell.setOnClickListener(this);

			// Log.d(tag, "**********Current Day: " + getCurrentDayOfMonth());
			String[] day_color = list.get(position).split("-");
			String theday = day_color[0];
			String themonth = day_color[2];
			String theyear = day_color[3];

			if ((!eventsPerMonthMap.isEmpty()) && (eventsPerMonthMap != null)) {
				if (eventsPerMonthMap.containsKey(theday)) {
					num_events_per_day = (TextView) row
							.findViewById(R.id.num_events_per_day);
					Integer numEvents = (Integer) eventsPerMonthMap.get(theday);
					num_events_per_day.setText(numEvents.toString());
				}

				Log.d(tag, "value of theday " + theday);
			}

			// Set the Day GridCell
			gridcell.setText(theday);
			// ****we set the TAG here
			gridcell.setTag(theday + "-" + themonth + "-" + theyear);
			String getEachGridDate = (String) gridcell.getTag();

			getgridText = (String) gridcell.getTag();

			if (day_color[1].equals("WHITE")) {
				gridcell.setTextColor(getResources().getColor(R.color.white));
			}

			/**
			 * if grid cell date EQUALS a 'taskdate' from the database, then
			 * carry out a colour change with colourChange() method.
			 */

			if (newDatesArrayString != null) {
				//Log.d("newDatesArrayString in if statement",
						//"newDatesArrayString" + newDatesArrayString);
				for (String d : newDatesArrayString) {

					if (getgridText.trim().equals(d)) {
						doColorChange();

						System.out.println("Notifcation " + d);

					}

					/**
					 * if grid cell date EQUALS a 'taskdate' from database AND
					 * it is the current date, then turn the text colour black
					 */

					if (getgridText.trim().equals(d)
							&& (day_color[1].equals("BLUE"))) {
						gridcell.setTextColor(getResources().getColor(
								R.color.black));

					}

				}// end of for loop

			}

			return row;

		}

		// change background of cell
		private void doColorChange() {
			gridcell.setBackgroundDrawable(null);
			gridcell.setBackgroundResource(R.drawable.calendar_bg_orange);

		}

		/**
		 * on clicking pass an intent which opens a TaskDayActivity class and
		 * pass the taskdate, instead of the task id. It opens a similar layout
		 * to tasksListActivity, but only shows tasks for a particular day (with the
		 * passed 'taskDate').
		 */

		@Override
		public void onClick(View view) {
			String date_month_year = (String) view.getTag();
			String date = date_month_year;

			// The method getTag() of the View class returns an Object stored in
			// this view as a tag
			Log.e("Selected date", date);
			try {
				Date parsedDate = dateFormatter.parse(date_month_year);
				// Log.d(tag, "Parsed Date: " + parsedDate.toString());
				Log.d(tag, "Parsed Date: " + parsedDate.toString());

			} catch (ParseException e) {
				e.printStackTrace();
			}

			// Starting new intent;
			Intent in = new Intent(getApplicationContext(),
					TaskDayActivity.class);
			// sending date to next activity
			in.putExtra(TAG_TASKDATE, date);

			// starting new activity and expecting some response back
			startActivityForResult(in, 100);
		}// end of onClick

		public int getCurrentDayOfMonth() {
			return currentDayOfMonth;
		}

		private void setCurrentDayOfMonth(int currentDayOfMonth) {
			this.currentDayOfMonth = currentDayOfMonth;
		}

		public void setCurrentWeekDay(int currentWeekDay) {
			this.currentWeekDay = currentWeekDay;
		}

		public int getCurrentWeekDay() {
			return currentWeekDay;
		}

	}// end of grid cell adapter class

	// get the results from the onPostExecute method of load_tasks and pass
	// back to main Calendar Activity
	@Override
	public void processFinish(String output) {

		newDatesArray = output;

		/**
		 * change into normal string array so we can loop through it in the
		 *  gridcell adapter class. need to get rid of first square bracket and turn into 
		 *  regular string array.
		 */
	
		if (newDatesArray != null) {
			
			newDatesArray = newDatesArray.substring(1,
					newDatesArray.length() - 1);
			newDatesArrayString = new ArrayList<String>(
					Arrays.asList(newDatesArray.split(",[ ]*")));

		}
		// update the gridcellAdapter
		this.adapter.notifyDataSetChanged();

	}// end of processFinish

	@Override
	public void processFinishNotification(String output) {
		// TODO Auto-generated method stub

	}

}// end of CalendarActivity class

