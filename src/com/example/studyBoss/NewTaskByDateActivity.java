package com.example.studyBoss;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class NewTaskByDateActivity extends Activity {

	// Progress Dialog
	private ProgressDialog pDialog;

	JSONParser jsonParser = new JSONParser();
	// Url to create new task
	private static String url_create_task = "http://fionawebdesign.co.uk/studyBoss/create_task.php";

	// JSON Node names. what we get back from jSON
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_TASK_DATE = "date";

	private String mPriorityResult;
	private EditText inputTitle;
	private EditText inputDate;
	private EditText inputDetail;
	private CheckBox mCheckBoxNotification;
	private CheckBox mCheckBoxComplete;
	private boolean mIsNotification;
	private boolean mIsComplete;
	// Place intent into this String variable
	private String taskDate;
	private RadioGroup mRadioProrityGroup;
	private RadioButton mRadioPriorityButton;
	private RadioButton mRadioLow;
	private RadioButton mRadioMid;
	private RadioButton mRadioHigh;
	private TextView mTextPriorityDisplay;
	public String aJsonString;
	final String KEY_SAVED_RADIO_BUTTON_INDEX = "SAVED_RADIO_BUTTON_INDEX";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_task);

		// Edit Text
		inputTitle = (EditText) findViewById(R.id.inputTitle);
		inputDate = (EditText) findViewById(R.id.inputDate);
		inputDetail = (EditText) findViewById(R.id.inputDetail);
		mCheckBoxNotification = (CheckBox) findViewById(R.id.task_notification);
		mCheckBoxComplete = (CheckBox) findViewById(R.id.task_list_item_completeCheckBox);

		Intent i = getIntent();

		// getting task date from intent so we can place into the EditText
		// inputDate form field
		taskDate = i.getStringExtra(TAG_TASK_DATE);
		// Log.d("taskDate", "taskDate = " + taskDate);

		inputDate.setText(taskDate);

		// set onCheck listener for notification checkbox
		mCheckBoxNotification.setOnCheckedChangeListener(

		new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// Log.d("Debug", "isChecked = " + Boolean.valueOf(isChecked));
				updateCheckBox();

			}

			// change value of 'mCheckBoxNotification' depending on whether
			// checkBox is checked or not?
			private void updateCheckBox() {
				if (mCheckBoxNotification.isChecked()) {
					// Log.d("Debug", "isChecked = " +
					// String.valueOf(isChecked));
					mIsNotification = true;

				} else {

					mIsNotification = false;
				}

				// Log.d("Debug", "isChecked = " + mIsNotification);
				Log.d("Debug", "isChecked = " + String.valueOf(mIsNotification));

			}
		});

		mCheckBoxComplete.setOnCheckedChangeListener(

		new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				// Log.d("Debug", "isChecked = " + Boolean.valueOf(isChecked));
				updateCompleteBox();

			}

			private void updateCompleteBox() {
				if (mCheckBoxComplete.isChecked()) {
					// Log.d("Debug", "isChecked = " +
					// String.valueOf(isChecked));
					mIsComplete = true;

				} else {

					mIsComplete = false;
				}

				// Log.d("Debug", "isChecked = " + mIsNotification);
				Log.d("Debug", "isChecked = " + String.valueOf(mIsComplete));

			}
		});

		// set onClickListerener on EditText inputDate, and create a date
		// dialogue.
		inputDate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				new DatePickerDialog(NewTaskByDateActivity.this, date,
						myCalendar.get(Calendar.YEAR), myCalendar
								.get(Calendar.MONTH), myCalendar
								.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

		/*------------- priority radio group -------------- */
		// working with the radio buttons
		mRadioProrityGroup = (RadioGroup) findViewById(R.id.radioPrority);
		mRadioLow = (RadioButton) findViewById(R.id.radioButtonLow);
		mRadioMid = (RadioButton) findViewById(R.id.radioButtonMid);
		mRadioHigh = (RadioButton) findViewById(R.id.radioButtonHigh);
		mTextPriorityDisplay = (TextView) findViewById(R.id.priorityTextDisplay);

		mRadioProrityGroup
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					public void onCheckedChanged(RadioGroup group, int checkedId) {

						// save the value of the radio button
						RadioButton checkedRadioButton = (RadioButton) mRadioProrityGroup
								.findViewById(checkedId);
						int checkedIndex = mRadioProrityGroup
								.indexOfChild(checkedRadioButton);

						// switch the radio button case and change values such
						// as drawable Resources accordingly
						switch (checkedId) {
						case R.id.radioButtonLow:

							mPriorityResult = "low";
							mTextPriorityDisplay
									.setCompoundDrawablesWithIntrinsicBounds(0,
											0, R.drawable.exclamation_circle, 0);
							mTextPriorityDisplay.setText("low priority");
							Log.d("Debug", "isChecked = " + mPriorityResult);

							break;

						case R.id.radioButtonMid:

							mPriorityResult = "medium";
							mTextPriorityDisplay
									.setCompoundDrawablesWithIntrinsicBounds(0,
											0, R.drawable.exclamation_mid, 0);
							mTextPriorityDisplay.setText("medium priority");
							Log.d("Debug", "isChecked = " + mPriorityResult);
							break;

						case R.id.radioButtonHigh:

							mPriorityResult = "high";
							mTextPriorityDisplay
									.setCompoundDrawablesWithIntrinsicBounds(0,
											0, R.drawable.high_circle, 0);
							mTextPriorityDisplay.setText("high priority");
							Log.d("Debug", "isChecked = " + mPriorityResult);
							break;
						}// end of switch

					}
				});

		// Create button
		Button btnCreateTask = (Button) findViewById(R.id.btnCreateTask);

		// button click event
		btnCreateTask.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// creating new task in background thread
				new CreateNewTask().execute();
			}
		});

		// Cancel button
		Button btnCancelTask = (Button) findViewById(R.id.btnCancelTask);

		// button click event
		btnCancelTask.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(getApplicationContext(),
						MainScreenActivity.class);
				startActivity(i);

			}
		});

	}

	Calendar myCalendar = Calendar.getInstance();

	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {

			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateLabel();
		}

	};

	// update inputDate Box depending on the date set on date dialogue
	private void updateLabel() {

		String myFormat = "MM/dd/yy";
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);
		inputDate.setText(sdf.format(myCalendar.getTime()));
	}

	/**
	 * Background Async Task to Create new task
	 * */
	class CreateNewTask extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(NewTaskByDateActivity.this);
			pDialog.setMessage("Creating your Task..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Creating task
		 * */
		protected String doInBackground(String... args) {
			String title = inputTitle.getText().toString();
			// get date value from intent
			String date = taskDate;
			String detail = inputDetail.getText().toString();
			String priority = mPriorityResult;
			String notification = String.valueOf(mIsNotification);
			String complete = String.valueOf(mIsComplete);

			// Building Parameters to send via http request
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("title", title));
			params.add(new BasicNameValuePair("date", date));
			params.add(new BasicNameValuePair("detail", detail));
			params.add(new BasicNameValuePair("priority", priority));
			params.add(new BasicNameValuePair("notification", notification));
			params.add(new BasicNameValuePair("complete", complete));

			// getting JSON Object
			// Note that create task url accepts POST method
			JSONObject json = jsonParser.makeHttpRequest(url_create_task,
					"POST", params);

			// check log cat for response
			Log.d("Create Response", json.toString());

			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// successfully created task
					// check for json message and pass to 'aJsonString' String
					if (json.has("message")) {
						aJsonString = json.getString("message");
					}
					Intent i = new Intent(getApplicationContext(),
							TaskListActivity.class);
					startActivity(i);

					// closing this screen
					finish();
				} else {
					// check for json message and pass to 'aJsonString
					if (json.has("message")) {
						aJsonString = json.getString("message");
					}
					// failed to create task
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once done
			pDialog.dismiss();

			// Add toast with json message response received from
			// doInBackground()
			LayoutInflater inflater = getLayoutInflater();
			// Inflate the Layout
			View layout = inflater.inflate(R.layout.my_custom_toast,
					(ViewGroup) findViewById(R.id.custom_toast_layout));

			TextView text = (TextView) layout.findViewById(R.id.textToShow);
			// Set the Text to show in TextView
			text.setText(aJsonString);

			Toast toast = new Toast(getApplicationContext());
			toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();

		}

	}
}
