package com.example.studyBoss;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import com.AsyncTasks.AddTask;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

public class NewTaskActivity extends Activity implements OnClickListener {

	// Progress Dialog
	private ProgressDialog pDialog;

	JSONParser jsonParser = new JSONParser();
	// Url to create new task
	private static String url_create_task = "http://fionawebdesign.co.uk/studyBoss/create_task.php";

	// JSON Node names. What we get back from jSON
	private static final String TAG_SUCCESS = "success";
	final String KEY_SAVED_RADIO_BUTTON_INDEX = "SAVED_RADIO_BUTTON_INDEX";

	private EditText inputTitle;
	private EditText inputDate;
	private EditText inputDetail;
	private CheckBox mCheckBoxNotification;
	private CheckBox mCheckBoxComplete;
	// make static so external Async can read the value.
	public static String mPriorityResult;
	public static boolean mIsNotification;
	public static boolean mIsComplete;

	private RadioGroup mRadioProrityGroup;
	private RadioButton mRadioPriorityButton;
	private RadioButton mRadioLow;
	private RadioButton mRadioMid;
	private RadioButton mRadioHigh;
	private TextView mTextPriorityDisplay;
	public String aJsonString;
	public String title;
	public String date;
	public String detail;
	public String priority;
	public String notification;
	public String complete;
	private AddTask mt;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_task);

		Button btnCreateTask = (Button) findViewById(R.id.btnCreateTask);
		btnCreateTask.setOnClickListener(this);

		// assign R.ids
		inputTitle = (EditText) findViewById(R.id.inputTitle);
		inputDate = (EditText) findViewById(R.id.inputDate);
		inputDetail = (EditText) findViewById(R.id.inputDetail);
		mCheckBoxNotification = (CheckBox) findViewById(R.id.task_notification);
		mCheckBoxComplete = (CheckBox) findViewById(R.id.task_list_item_completeCheckBox);

		// set onCheck listener for notification checkbox
		mCheckBoxNotification.setOnCheckedChangeListener(

		new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// Log.d("Debug", "isChecked = " + Boolean.valueOf(isChecked));
				updateCheckBox();

			}

			// Change value of 'mIsNotification' according to whether checkBox
			// is checked or not.
			private void updateCheckBox() {
				if (mCheckBoxNotification.isChecked()) {
					// Log.d("Debug", "isChecked = " +
					// String.valueOf(isChecked));
					mIsNotification = true;

				} else {

					mIsNotification = false;
				}

				// Log.d("Debug", "isChecked = " + mIsNotification);
				Log.d("Debug", "isChecked = " + String.valueOf(mIsNotification));

			}
		});

		// set oncheck listener for CheckBoxCompletecheckbox
		mCheckBoxComplete.setOnCheckedChangeListener(

		new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// call the update method
				updateCompleteBox();

			}

			private void updateCompleteBox() {
				if (mCheckBoxComplete.isChecked()) {
					// Log.d("Debug", "isChecked = " +
					// String.valueOf(isChecked));
					mIsComplete = true;

				} else {

					mIsComplete = false;
				}

				Log.d("Debug", "isChecked = " + String.valueOf(mIsComplete));

			}
		});

		inputDate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				new DatePickerDialog(NewTaskActivity.this, Diogdate, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

		/*------------- priority radio group -------------- */
		// working with the radio buttons for Task Priority
		mRadioProrityGroup = (RadioGroup) findViewById(R.id.radioPrority);
		mRadioLow = (RadioButton) findViewById(R.id.radioButtonLow);
		mRadioMid = (RadioButton) findViewById(R.id.radioButtonMid);
		mRadioHigh = (RadioButton) findViewById(R.id.radioButtonHigh);
		mTextPriorityDisplay = (TextView) findViewById(R.id.priorityTextDisplay);

		mRadioProrityGroup
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					public void onCheckedChanged(RadioGroup group, int checkedId) {

						// assign value of the radio button group checked ID
						RadioButton checkedRadioButton = (RadioButton) mRadioProrityGroup
								.findViewById(checkedId);
						int checkedIndex = mRadioProrityGroup
								.indexOfChild(checkedRadioButton);

						// switch condition for the radio button ID
						// change the text and the background image depending on
						// the switch case
						switch (checkedId) {
						case R.id.radioButtonLow:

							mPriorityResult = "low";
							// change background drawable
							mTextPriorityDisplay
									.setCompoundDrawablesWithIntrinsicBounds(0,
											0, R.drawable.exclamation_circle, 0);
							// change text
							mTextPriorityDisplay.setText("low priority");
							Log.d("Debug", "isChecked = " + mPriorityResult);

							break;

						case R.id.radioButtonMid:

							mPriorityResult = "medium";
							mTextPriorityDisplay
									.setCompoundDrawablesWithIntrinsicBounds(0,
											0, R.drawable.exclamation_mid, 0);
							mTextPriorityDisplay.setText("medium priority");
							Log.d("Debug", "isChecked = " + mPriorityResult);
							break;

						case R.id.radioButtonHigh:

							mPriorityResult = "high";
							mTextPriorityDisplay
									.setCompoundDrawablesWithIntrinsicBounds(0,
											0, R.drawable.high_circle, 0);
							mTextPriorityDisplay.setText("high priority");
							Log.d("Debug", "isChecked = " + mPriorityResult);
							break;
						}// end of switch

					}
				});

		// Cancel button
		Button btnCancelTask = (Button) findViewById(R.id.btnCancelTask);

		// button click event
		btnCancelTask.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent i = new Intent(getApplicationContext(),
						MainScreenActivity.class);
				startActivity(i);

			}
		});
	}

	Calendar myCalendar = Calendar.getInstance();

	DatePickerDialog.OnDateSetListener Diogdate = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {

			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			// call updateLabel() method
			updateLabel();
		}

	};

	// update text in inputDate
	private void updateLabel() {

		String myFormat = "d-MMMM-yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);

		inputDate.setText(sdf.format(myCalendar.getTime()));
	}

	@Override
	public void onClick(View v) {
		// create a new instance of AddTask class and execute
		// This will add the new task
		AddTask NewTaskAsync = new AddTask(this);
		NewTaskAsync.execute();

	}

}
