package com.example.studyBoss;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class EditTaskActivity extends Activity {

	// JSON parser class
	JSONParser jsonParser = new JSONParser();

	// single product url
	private static String url_task_details = "http://fionawebdesign.co.uk/studyBoss/get_task_details.php";
	private static String url_update_task = "http://fionawebdesign.co.uk/studyBoss/update_task.php";
	private static String url_delete_task = "http://fionawebdesign.co.uk/studyBoss/delete_task.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_TASK = "task";
	private static final String TAG_PID = "pid";
	private static final String TAG_TITLE = "title";
	private static final String TAG_DATE = "date";
	private static final String TAG_DETAIL = "detail";
	private static final String TAG_PRIORITY = "priority";
	private static final String TAG_NOTIFICATION = "notification";
	private static final String TAG_COMPLETE = "complete";
	// class properties
	private EditText txtTitle;
	private EditText txtDate;
	private EditText txtDetail;
	private EditText txtCreatedAt;
	private EditText txtPriority;
	private TextView checkNotification;
	private TextView mPriorityLabel;
	private CheckBox mCheckBoxNotification;
	private CheckBox mCheckBoxComplete;
	private String notificationResult;
	private String completeResult;
	private String mPriorityResult;
	private Button btnSave;
	private Button btnDelete;
	private Button btnNotification;
	private RadioGroup mRadioProrityGroup;
	private RadioButton mRadioPriorityButton;
	private TextView mTextPriorityDisplay;
	private RadioButton mRadioLow;
	private RadioButton mRadioMid;
	private RadioButton mRadioHigh;
	private ImageView mPriorityDisplay;
	private String testPriority;
	public String pid;
	final String KEY_SAVED_RADIO_BUTTON_INDEX = "SAVED_RADIO_BUTTON_INDEX";
	// Progress Dialog
	private ProgressDialog pDialog;
	// boolean values for saving the data
	private boolean mIsNotification;
	private boolean mIsComplete;
	private String mSavedPriority;
	private Boolean checkBoolNoification;
	private Boolean checkBoolComplete;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_task);

		// buttons
		btnSave = (Button) findViewById(R.id.btnSave);
		btnDelete = (Button) findViewById(R.id.btnDelete);
		txtDate = (EditText) findViewById(R.id.inputDate);
		mCheckBoxNotification = (CheckBox) findViewById(R.id.task_notification);
		mCheckBoxComplete = (CheckBox) findViewById(R.id.task_list_item_completeCheckBox);

		// Get intent
		Intent i = getIntent();

		// Get task id (pid) from intent
		pid = i.getStringExtra(TAG_PID);

		// Get all the taskdetails in background thread
		new GetTaskDetails().execute();

		// set onCheck listener for notification checkbox
		mCheckBoxNotification.setOnCheckedChangeListener(

		new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// Log.d("Debug", "isChecked = " + Boolean.valueOf(isChecked));
				// update the checkBox
				updateCheckBox();

			}

			// update value of 'mIsNotification' when checkbox state is changed
			private void updateCheckBox() {
				if (mCheckBoxNotification.isChecked()) {
					mIsNotification = true;
				} else {
					mIsNotification = false;
				}

				// Log.d("Debug", "isChecked = " + mIsNotification);
				Log.d("Debug", "isChecked = " + String.valueOf(mIsNotification));

			}
		});

		// set onCheck listener for Complete checkbox
		mCheckBoxComplete.setOnCheckedChangeListener(

		new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				updateCompleteBox();

			}

			private void updateCompleteBox() {
				if (mCheckBoxComplete.isChecked()) {
					// Log.d("Debug", "isChecked = " +
					// String.valueOf(isChecked));
					mIsComplete = true;

				} else {
					mIsComplete = false;
				}

				// Log.d("Debug", "isChecked = " + mIsNotification);
				Log.d("Debug", "isChecked = " + String.valueOf(mIsComplete));

			}
		});

		// working with the radio buttons
		mRadioProrityGroup = (RadioGroup) findViewById(R.id.radioPrority);
		mRadioLow = (RadioButton) findViewById(R.id.radioButtonLow);
		mRadioMid = (RadioButton) findViewById(R.id.radioButtonMid);
		mRadioHigh = (RadioButton) findViewById(R.id.radioButtonHigh);
		mTextPriorityDisplay = (TextView) findViewById(R.id.priorityTextDisplay);

		mRadioProrityGroup
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					public void onCheckedChanged(RadioGroup group, int checkedId) {

						// save the value of the radio button
						RadioButton checkedRadioButton = (RadioButton) mRadioProrityGroup
								.findViewById(checkedId);
						int checkedIndex = mRadioProrityGroup
								.indexOfChild(checkedRadioButton);

						// switch condition to change the mPriorityResult value,
						// the text
						// on the priority display and the background drawable
						// depending on which radio button is selected
						switch (checkedId) {
						case R.id.radioButtonLow:

							mPriorityResult = "low";
							mTextPriorityDisplay
									.setCompoundDrawablesWithIntrinsicBounds(0,
											0, R.drawable.exclamation_circle, 0);
							mTextPriorityDisplay.setText("low priority");
							// Log.d("Debug", "isChecked = " + testPriority);
							break;

						case R.id.radioButtonMid:

							mPriorityResult = "medium";
							mTextPriorityDisplay
									.setCompoundDrawablesWithIntrinsicBounds(0,
											0, R.drawable.exclamation_mid, 0);
							mTextPriorityDisplay.setText("medium priority");
							// Log.d("Debug", "isChecked = " + mPriorityResult);
							break;

						case R.id.radioButtonHigh:

							mPriorityResult = "high";
							mTextPriorityDisplay
									.setCompoundDrawablesWithIntrinsicBounds(0,
											0, R.drawable.high_circle, 0);
							mTextPriorityDisplay.setText("high priority");
							// Log.d("Debug", "isChecked = " +mPriorityResult);
							break;

						}// end of switch

					}
				});

		// save button click event
		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Start background process to save the task (inner Async class
				// below)
				new SaveTaskDetails().execute();
			}
		});

		// Delete button click event
		btnDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Start background process to delete the task (inner Async
				// class below)
				new DeleteTask().execute();
			}
		});

		// set an onclick listener on dateinput and create a date picker
		// dialogue
		txtDate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				new DatePickerDialog(EditTaskActivity.this, date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

	}// end of onCreate

	Calendar myCalendar = Calendar.getInstance();
	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {

			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateLabel();
		}

	};

	// update the text in the input date box and make sure it is formatted
	// correctly
	private void updateLabel() {

		String myFormat = "d-MMMM-yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);
		txtDate.setText(sdf.format(myCalendar.getTime()));
	}

	/**
	 * Background Async Task to Get all task details from remote database
	 * */
	class GetTaskDetails extends AsyncTask<String, String, String> {

		// Show the Progress Dialog before starting the background thread
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EditTaskActivity.this);
			pDialog.setMessage("Loading your task details. Please wait...and thankyou..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		// Get the task details in the background thread

		protected String doInBackground(String... params) {

			// update UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					// Check for success tag
					int success;
					try {
						// Building Parameters
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("pid", pid));

						// getting task details by making HTTP request
						// Note that task details url will use GET request
						JSONObject json = jsonParser.makeHttpRequest(
								url_task_details, "GET", params);

						// Log.d("Single Task Details", json.toString());

						// Json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
							// Have successfully received task details, get the
							// json Array and place into productObj
							JSONArray productObj = json.getJSONArray(TAG_TASK); // JSON
																				// Array

							// get first task object from JSON Array
							JSONObject task = productObj.getJSONObject(0);

							/*
							 * task with this pid found. Assign the view
							 * elements I will fill with the data
							 */

							txtTitle = (EditText) findViewById(R.id.inputTitle);
							txtDate = (EditText) findViewById(R.id.inputDate);
							txtDetail = (EditText) findViewById(R.id.inputDetail);
							txtPriority = (EditText) findViewById(R.id.inputPriority);
							checkNotification = (TextView) findViewById(R.id.notificationDetails);
							mCheckBoxNotification = (CheckBox) findViewById(R.id.task_notification);
							mCheckBoxComplete = (CheckBox) findViewById(R.id.task_list_item_completeCheckBox);
							mRadioProrityGroup = (RadioGroup) findViewById(R.id.radioPrority);
							mRadioLow = (RadioButton) findViewById(R.id.radioButtonLow);
							mRadioMid = (RadioButton) findViewById(R.id.radioButtonMid);
							mRadioHigh = (RadioButton) findViewById(R.id.radioButtonHigh);
							mTextPriorityDisplay = (TextView) findViewById(R.id.priorityTextDisplay);
							mPriorityLabel = (TextView) findViewById(R.id.priorityLabel);

							// display the data in the assigned view element
							txtTitle.setText(task.getString(TAG_TITLE));
							txtDate.setText(task.getString(TAG_DATE));
							txtDetail.setText(task.getString(TAG_DETAIL));
							// store the priority value in a variable string
							mSavedPriority = task.getString(TAG_PRIORITY);
							txtPriority.setText(mSavedPriority);
							mPriorityLabel.setText("TASK PRIORITY:"
									+ mSavedPriority.toUpperCase());
							notificationResult = task
									.getString(TAG_NOTIFICATION);
							checkNotification.setText(notificationResult);
							// need to change the notification result to a
							// Boolean
							checkBoolNoification = Boolean
									.valueOf(notificationResult);
							mCheckBoxNotification
									.setChecked(checkBoolNoification);
							completeResult = task.getString(TAG_COMPLETE);
							checkBoolComplete = Boolean.valueOf(completeResult);
							// need to change the notification result to a
							// Boolean
							mCheckBoxComplete.setChecked(checkBoolComplete);

							// keep the correct checkbox selected
							if (mSavedPriority.equalsIgnoreCase("high")) {
								mRadioHigh.setChecked(true);

							}

							else if (mSavedPriority.equalsIgnoreCase("medium")) {
								mRadioMid.setChecked(true);

							} else if (mSavedPriority.equalsIgnoreCase("low")) {
								mRadioLow.setChecked(true);

							}

						} else {
							// task with pid not found
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		// After the background task is complete we can dismiss the progress
		// dialog

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			pDialog.dismiss();
		}
	}

	/*****************************************************************/
	// Background Async Task to Save the task Details

	class SaveTaskDetails extends AsyncTask<String, String, String> {

		// Display Progress Dialog

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EditTaskActivity.this);
			pDialog.setMessage("Saving your task...so now you have no excuse!");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		// Save Task

		protected String doInBackground(String... args) {

			// getting updated data from EditTexts
			String title = txtTitle.getText().toString();
			String date = txtDate.getText().toString();
			String detail = txtDetail.getText().toString();
			String priority = mPriorityResult;
			String notification = String.valueOf(mIsNotification);
			String complete = String.valueOf(mIsComplete);

			// Building the parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(TAG_PID, pid));
			params.add(new BasicNameValuePair(TAG_TITLE, title));
			params.add(new BasicNameValuePair(TAG_DATE, date));
			params.add(new BasicNameValuePair(TAG_DETAIL, detail));
			params.add(new BasicNameValuePair(TAG_PRIORITY, priority));
			params.add(new BasicNameValuePair(TAG_NOTIFICATION, notification));
			params.add(new BasicNameValuePair(TAG_COMPLETE, complete));

			// sending modified data through http request

			// The update task url accepts a POST method
			JSONObject json = jsonParser.makeHttpRequest(url_update_task,
					"POST", params);

			// check json success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// successfully updated
					Intent i = getIntent();
					// send result code 100 to notify about task update
					setResult(100, i);
					finish();
				} else {
					// failed to update task
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		// Dismiss the progress dialog once the background task is completed

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
		}
	}

	/*****************************************************************/
	// Background Async Task to Delete Task

	class DeleteTask extends AsyncTask<String, String, String> {

		// Show Progress Dialog

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EditTaskActivity.this);
			pDialog.setMessage("Deleting Task...it no longer needs to be done!");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		// Deleting task

		protected String doInBackground(String... args) {

			// Check for success tag
			int success;
			try {
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				// second parameter is what you add in.
				params.add(new BasicNameValuePair("pid", pid));
				// get task details with a HTTP request POST method

				JSONObject json = jsonParser.makeHttpRequest(url_delete_task,
						"POST", params);

				// check your log for json response
				Log.d("Delete Task", json.toString());

				// json success tag
				success = json.getInt(TAG_SUCCESS);
				if (success == 1) {
					// Task successfully deleted
					// notify previous activity by sending code 100
					Intent i = getIntent();
					// send result code 100 to notify about product deletion
					setResult(100, i);
					finish();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		// Dismiss the progress dialog

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			pDialog.dismiss();

		}// end of PostExeute
	}

}// end of class
