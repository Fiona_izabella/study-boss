package com.example.studyBoss;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class TaskListActivity extends ListActivity {

	// Progress Dialog
	private ProgressDialog pDialog;
	// Creating JSON Parser object
	JSONParser jParser = new JSONParser();
	public ArrayList<HashMap<String, String>> tasksList;
	public ArrayList<String> datesNewArray;
	// tasks JSONArray
	JSONArray tasks = null;
	// URL to get all Tasks
	private static String url_all_tasks = "http://fionawebdesign.co.uk/studyBoss/get_all_tasks.php";
	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_TASKS = "tasks";
	private static final String TAG_PID = "pid";
	private static final String TAG_NAME = "title";
	private static final String TAG_DATE = "date";
	private static final String TAG_COMPLETE = "complete";
	private static final String TAG_PRIORITY = "priority";
	private static final String[] ItemValue = null;
	private String mPriorityResult;
	private String todaysDate;
	private String mImage_bg;
	private String complete;
	private String date;
	private String title;
	private String id;
	private String completeResultTest;
	public String dateClicked;
	public ArrayList<String> datesArray;
	private boolean completeResult;
	private boolean mIsComplete;
	// dates elements
	private TextView mDisplayCurrentDate;
	private int mYear;
	private int mMonth;
	private int mDay;
	private int month;
	// buttons/checkbox/imageview
	private ImageView priorityDisplay;
	private Button btnTestdate;
	public CheckBox mCompleteCheckBox;
	// Listview
	private ListView lv;
	private ListAdapter adapter;
	// buttons
	private Button headerBtn;
	private Button homeBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// add custom title bar (header.xml)
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.all_tasks);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.header);

		// display current date
		mDisplayCurrentDate = (TextView) findViewById(R.id.display_Date);

		final Calendar c = Calendar.getInstance();
		Date date = c.getTime();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		c.set(Calendar.YEAR, mYear);
		c.set(Calendar.MONTH, mMonth);
		c.set(Calendar.DAY_OF_MONTH, mDay);

		String myFormat = "dd-MMMM-yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);

		mDisplayCurrentDate.setText(sdf.format(c.getTime()));
		todaysDate = (String) mDisplayCurrentDate.getText();
		Log.d("todaysDate", "todays date " + todaysDate);

		// Two buttons in custom title bar
		headerBtn = (Button) findViewById(R.id.header_right_btn);
		headerBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// start CalendarActivity class
				Intent j = new Intent(getApplicationContext(),
						CalendarActivity.class);

				startActivity(j);
			}
		});

		homeBtn = (Button) findViewById(R.id.header_right_lft);
		homeBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// start MainScreenActivity class
				Intent k = new Intent(getApplicationContext(),
						MainScreenActivity.class);

				startActivity(k);
			}
		});

		// Hashmap for ListView
		tasksList = new ArrayList<HashMap<String, String>>();
		// Loading tasks in Background Thread
		new LoadAllTasks().execute();

		// Get listview
		lv = getListView();

		// When you select a single Task
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// Get id of selected task
				String pid = ((TextView) view.findViewById(R.id.pid)).getText()
						.toString();

				// Start new intent
				Intent in = new Intent(getApplicationContext(),
						EditTaskActivity.class);
				// sending pid (Task ID) to next activity
				in.putExtra(TAG_PID, pid);

				// starting new activity and expecting some response back
				startActivityForResult(in, 100);
			}
		});

	}// end of onCreate

	// Response from EditTaskActivity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received
			// means user edited/deleted task successfully and then reload the
			// screen
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}

	}

	/**
	 * Background Async Task to Load all tasks by making HTTP Request with GET
	 * eg.. class LoadAllTasks extends AsyncTask<X, Y, Z>
	 * */
	class LoadAllTasks extends AsyncTask<String, String, String> {
		/**
		 * Before starting background thread Show Progress Dialog this method is
		 * specific so needs to stay in this activity
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(TaskListActivity.this);
			pDialog.setMessage("Loading tasks. Please wait...thankyou");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All tasks from URL
		 * */

		// protected Z doInBackground(X...x).. returns Z
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_all_tasks, "GET",
					params);

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// tasks are found
					// Getting Array of Tasks
					tasks = json.getJSONArray(TAG_TASKS);

					// looping through All Tasks
					for (int i = 0; i < tasks.length(); i++) {
						JSONObject taskDetail = tasks.getJSONObject(i);

						// Storing each json item in variable
						id = taskDetail.getString(TAG_PID);
						title = taskDetail.getString(TAG_NAME);
						date = taskDetail.getString(TAG_DATE);
						complete = taskDetail.getString(TAG_COMPLETE);
						completeResult = Boolean.valueOf(complete);

						// set the text we want to see for whether a task is
						// complete or not
						if (completeResult) {
							completeResultTest = "Complete";
						} else {
							completeResultTest = "Need to do!";
						}

						mPriorityResult = taskDetail.getString(TAG_PRIORITY);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();
						// adding each child node to HashMap key => value
						map.put(TAG_PID, id);
						map.put(TAG_NAME, title);
						map.put(TAG_DATE, date);
						map.put(TAG_COMPLETE, completeResultTest);
						map.put(TAG_PRIORITY, mPriorityResult);

						// adding HashList to tasksListArray
						tasksList.add(map);

					}

				} else {

					// no tasks found
					// Launch Add NewTaskActivity
					Intent i = new Intent(getApplicationContext(),
							NewTaskActivity.class);
					// Closing all previous activities
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * //Dismiss the progress dialog. Depending on value returned from
		 * doInBackground // run the runnable method and use the custom simple
		 * adapter to // create and fill the listView values
		 * **/
		// protected void onPostExecute(Z z){
		// As an input parameter you receive the output parameter of the
		// doInBackground method.
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all tasks
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

					// Updating parsed JSON data into ListView
					adapter = new CustomAdapter(TaskListActivity.this,
							tasksList, R.layout.list_item, new String[] {
									TAG_PID, TAG_NAME, TAG_DATE, TAG_COMPLETE,
									TAG_PRIORITY },

							new int[] { R.id.pid, R.id.title,
									R.id.task_list_item_dateTextView,
									R.id.task_list_item_complete,
									R.id.priority_text });

					setListAdapter(adapter);

				}

			});

		}

	}

	/* ===== custom adapter ======= */

	@SuppressLint("NewApi")
	public class CustomAdapter extends SimpleAdapter {
		public CustomAdapter(Context context,
				List<? extends Map<String, ?>> data, int resource,
				String[] from, int[] to) {
			super(context, data, resource, from, to);

		}

		@SuppressWarnings("deprecation")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = super.getView(position, convertView, parent);
			Object item = getItem(position);
			ColorStateList color = getResources().getColorStateList(
					R.drawable.text_color);
			ColorStateList colorHighlight = getResources().getColorStateList(
					R.drawable.text_colour_emphasize);
			// get listView and adapter count
			int countChild = lv.getChildCount();
			int adapterCount = lv.getCount();
			// loop through adapter list count to find listview values
			for (int i = 0; i < adapterCount; i++) {

				TextView priorityText = (TextView) v
						.findViewById(R.id.priority_text);
				TextView currentDate = (TextView) v
						.findViewById(R.id.task_list_item_dateTextView);
				TextView completeState = (TextView) v
						.findViewById(R.id.task_list_item_complete);

				String d = (String) currentDate.getText();
				String p = (String) priorityText.getText();
				String c = (String) completeState.getText();

				/*
				 * change drawable background resources of list view depending
				 * on listItem values. Need 'else' solution, otherwise the first
				 * item in list doesn't respond properly
				 */

				if (d.equalsIgnoreCase(todaysDate)) {
					currentDate.setTextColor(colorHighlight);
					currentDate.setCompoundDrawablesWithIntrinsicBounds(
							R.drawable.bell, 0, 0, 0);

				}

				else {

					currentDate.setTextColor(color);
					// else set the background resource to nothing.
					currentDate.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							0, 0);

				}

				if (p.equalsIgnoreCase("high")) {

					priorityText.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.high_circle, 0);

				} else if (p.equalsIgnoreCase("medium")) {
					priorityText.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.exclamation_mid, 0);

				} else {

					priorityText.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.exclamation_circle, 0);
				}

				if (c.equalsIgnoreCase("Complete")) {

					completeState.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.star_green, 0);

				} else {

					completeState.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.star_o, 0);

				}

			}

			return v;
		}
	}

}// end of class