package com.AsyncTasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.studyBoss.AsyncResponse;
import com.example.studyBoss.JSONParser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Background Async Task to Load all task dates by making HTTP Request. The
 * results are passed on from the onPostExecute to the calendarActivity.
 * */
public class Load_dates extends AsyncTask<String, String, String> {

	public AsyncResponse delegate = null;

	JSONParser jParser = new JSONParser();
	private static String url_all_dates = "http://fionawebdesign.co.uk/studyBoss/get_dates.php";
	// Progress Dialog
	private ProgressDialog pDialog;
	// JSON Node names. eg {"date" : "31-March-2014"}
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_DATES = "dates";
	private static final String TAG_DATE = "date";
	// tasks JSONArray
	JSONArray dates = null;

	public String date;
	public String jsonResult;
	public ArrayList<HashMap<String, String>> datesList;
	public ArrayList<String> stringArray = new ArrayList<String>();
	public String doInBackResult;

	// Get All tasks dates from url
	protected String doInBackground(String... args) {

		datesList = new ArrayList<HashMap<String, String>>();
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// getting JSON string from URL with GET
		JSONObject json = jParser.makeHttpRequest(url_all_dates, "GET", params);

		try {
			// Checking for SUCCESS TAG

			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// tasks are found
				// Getting the json array. dates = [ {}, {], ]
				dates = json.getJSONArray(TAG_DATES);

				jsonResult = dates.toString();

				// Log jsonResult
				// Log.d("All Dates: ", jsonResult);

				/**
				 * looping through All Tasks in the array and placing each json
				 * object {} from inside the array into a separate object {}
				 * called taskdetail
				 */

				for (int i = 0; i < dates.length(); i++) {
					JSONObject taskDetail = dates.getJSONObject(i);

					/**
					 * Storing each json item into taskdetail obj, which is
					 * assigned to a variable called date this gives a single
					 * date object which are added to 'stringArray'
					 */

					date = taskDetail.getString(TAG_DATE);
					stringArray.add(date.toString());
					// doInBackground() method will return doInBackResult
					doInBackResult = stringArray.toString();
					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();
					// adding each child node to HashMap key => value
					// ****add each json object {} to the datesList array
					map.put(TAG_DATE, date);
					datesList.add(map);
				}
				jsonResult = datesList.toString();

			} else {

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return doInBackResult;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	public void onPostExecute(final String result) {

		delegate.processFinish(result);

	}

}// end of class

