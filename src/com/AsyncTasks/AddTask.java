package com.AsyncTasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.studyBoss.JSONParser;
import com.example.studyBoss.NewTaskActivity;
import com.example.studyBoss.R;
import com.example.studyBoss.TaskListActivity;
import com.example.studyBoss.R.id;
import com.example.studyBoss.R.layout;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class AddTask extends AsyncTask<String, String, String> {

	// When creating the AsyncTask, supply the current Activity to the
	// constructor. Pass the Context to the Constructor of the AsyncTask.

	// Progress Dialog
	private ProgressDialog pDialog;

	JSONParser jsonParser = new JSONParser();
	// Url to create new task
	private static String url_create_task = "http://fionawebdesign.co.uk/studyBoss/create_task.php";
	// JSON Node names. What we get back from jSON
	private static final String TAG_SUCCESS = "success";
	final String KEY_SAVED_RADIO_BUTTON_INDEX = "SAVED_RADIO_BUTTON_INDEX";

	private EditText inputTitle;
	private EditText inputDate;
	private EditText inputDetail;
	private CheckBox mCheckBoxNotification;
	private CheckBox mCheckBoxComplete;
	public boolean mIsNotification;
	public boolean mIsComplete;
	public String mPriorityResult;
	private RadioGroup mRadioProrityGroup;
	private RadioButton mRadioPriorityButton;
	private RadioButton mRadioLow;
	private RadioButton mRadioMid;
	private RadioButton mRadioHigh;
	private TextView mTextPriorityDisplay;
	public String aJsonString;
	public String title;
	public String date;
	public String detail;
	public String priority;
	public String notification;
	public String complete;

	Context c;

	public AddTask(Context c) {
		this.c = c;
	}

	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(c);
		pDialog.setMessage("Creating your Task..");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
	}

	/**
	 * Creating task. Need to pass the context of the Activity (c)
	 * */
	protected String doInBackground(String... args) {

		// assign R.ids
		inputTitle = (EditText) ((Activity) c).findViewById(R.id.inputTitle);
		inputDate = (EditText) ((Activity) c).findViewById(R.id.inputDate);
		inputDetail = (EditText) ((Activity) c).findViewById(R.id.inputDetail);
		mCheckBoxNotification = (CheckBox) ((Activity) c)
				.findViewById(R.id.task_notification);
		mCheckBoxComplete = (CheckBox) ((Activity) c)
				.findViewById(R.id.task_list_item_completeCheckBox);

		title = inputTitle.getText().toString();
		date = inputDate.getText().toString();
		detail = inputDetail.getText().toString();
		priority = NewTaskActivity.mPriorityResult;
		Boolean notificationFromTasks = NewTaskActivity.mIsNotification;
		notification = String.valueOf(notificationFromTasks);
		Boolean completeFromTasks = NewTaskActivity.mIsComplete;
		complete = String.valueOf(completeFromTasks);

		// Building Parameters to send via http request
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("title", title));
		params.add(new BasicNameValuePair("date", date));
		params.add(new BasicNameValuePair("detail", detail));
		params.add(new BasicNameValuePair("priority", priority));
		params.add(new BasicNameValuePair("notification", notification));
		params.add(new BasicNameValuePair("complete", complete));

		// getting JSON Object
		// Note that create task url accepts POST method
		JSONObject json = jsonParser.makeHttpRequest(url_create_task, "POST",
				params);

		// check for success tag
		try {
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// successfully created task
				// check for json message and pass to 'aJsonString
				if (json.has("message")) {
					aJsonString = json.getString("message");
				}
				Intent i = new Intent(c, TaskListActivity.class);
				c.startActivity(i);
				// closing this screen
				((Activity) this.c).finish();

			} else {
				// check for json message and pass to 'aJsonString
				if (json.has("message")) {
					aJsonString = json.getString("message");
				}
				// failed to create task
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
		// dismiss the dialog once done
		pDialog.dismiss();

		// Add toast with json message form doInBackground()
		LayoutInflater inflater = (LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// LayoutInflater inflater = getLayoutInflater();
		// Inflate the Layout
		View layout = inflater.inflate(R.layout.my_custom_toast,
				(ViewGroup) ((Activity) c)
						.findViewById(R.id.custom_toast_layout));

		TextView text = (TextView) layout.findViewById(R.id.textToShow);
		// Set the Text to show in TextView
		text.setText(aJsonString);

		Toast toast = new Toast(c);
		toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();

	}
}
