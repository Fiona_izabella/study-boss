package com.AsyncTasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.studyBoss.AsyncResponse;
import com.example.studyBoss.JSONParser;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

//class which extends AsyncTask and returns notification results from the remote database
public class GetNotificationResults extends AsyncTask<String, String, String> {

	public AsyncResponse delegate = null;
	// variables
	private static final String TAG_DATES = "dates";
	private static final String TAG_TASKDATE = "date";
	private static final String TAG_SUCCESS = "success";
	public static String datesArray;
	public String jsonResult;
	public String notificationDate;
	public String date;
	public String backGroundResult;
	public String notificationDatesArray;
	public ArrayList<String> notificationDatesArrayString;
	private static final String TAG_DATE = "date";
	public ArrayList<HashMap<String, String>> datesListNotifications;
	public ArrayList<String> dateStringArray = new ArrayList<String>();
	// new JsonParse Object
	JSONParser jParser = new JSONParser();

	private static String url_dates_byNotification = "http://fionawebdesign.co.uk/studyBoss/get_dates_by_notification.php";
	// tasks JSONArray
	JSONArray dates = null;
	// Progress Dialog
	private ProgressDialog pDialog;

	// getting All tasks from url
	protected String doInBackground(String... args) {

		datesListNotifications = new ArrayList<HashMap<String, String>>();
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// getting JSON string from URL with GET
		JSONObject json = jParser.makeHttpRequest(url_dates_byNotification,
				"GET", params);

		 //Log.d("All Dates: ", json.toString());

		try {
			// Checking for SUCCESS TAG
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// tasks are found
				// Getting the json array. dates = [ {}, {], ]
				dates = json.getJSONArray(TAG_DATES);

				jsonResult = dates.toString();

				//Log.d("All Dates: ", jsonResult);
				/**
				 * looping through All Dates in the array and placing each json
				 * object {} from inside the array
				 * into a separate object {} called taskdetail
				 */
							
				for (int i = 0; i < dates.length(); i++) {
					JSONObject taskDate = dates.getJSONObject(i);

					date = taskDate.getString(TAG_DATE);
					dateStringArray.add(date.toString());

					// this will be passed as the result
					backGroundResult = dateStringArray.toString();

					/*
					 * Important Testing logs
					 */
					// Log.d("stringArrayDates: ", stringArray.toString());
					//Log.d("doInBackResult ", backGroundResult);

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					map.put(TAG_DATE, date);
					datesListNotifications.add(map);

				}
				jsonResult = datesListNotifications.toString();

			} else {

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}catch (Exception e) {
			System.err.println( "EXCEPTION THROWN LOADING JSON");
			e.printStackTrace();
		}

		return backGroundResult;
	}

	public void onPostExecute(final String result) {

		delegate.processFinishNotification(result);

	}// end of onPostExecute

}
