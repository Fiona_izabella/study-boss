**A small project for an android application i built in university in 2014.**

The application connects with a table on a remote server database.
e.g. http://fionawebdesign.co.uk/studyBoss/get_all_tasks.php

So the application should work straight away, as long as there is a good internet connection. (The application will work slow with a bad internet connection).

The php files that are placed on a remote server (http://fionawebdesign.co.uk/studyBoss/) can be 
viewed in the folder called 'php', along with the sql file used to create the table.


For the application to work locally:

If the application is to work locally, all the links in the Activities which connect to the database 
will have to change from:
(examples)
private static String url_all_dates = "http://fionawebdesign.co.uk/studyBoss/get_dates.php";
to 
private static String url_all_dates = "http://192.168.1.201:8888/studyBossConnect/get_dates.php";

The php db_connect and db_config files will need to change to a new database address.
These will have to change from:
define('DB_USER', "studyBoss"); // db user
define('DB_PASSWORD', "studyBoss"); // db password (mention your db password here)
define('DB_DATABASE', "studyBoss"); // database name
define('DB_SERVER', "cust-mysql-123-19"); // db server

to 

define('DB_USER', "root"); // db user
define('DB_PASSWORD', "root"); // db password (mention your db password here)
define('DB_DATABASE', "studyBoss"); // database name
define('DB_SERVER', "localhost"); // db server


And each php file will need to change the connection code from:
define('__ROOT__', dirname(dirname(__FILE__))); 
        require_once(__ROOT__.'/studyBoss/db_config.php');

to 

require_once __DIR__ . '/db_config.php';